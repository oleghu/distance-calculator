import "./App.css";
import React from "react";
import { useState, useEffect } from "react";

function App() {
  const [firstCity, setFirstCity] = useState("Oslo");
  const [secondCity, setSecondCity] = useState("Bergen");
  const [firstCityData, setFirstCityData] = useState(JSON);
  const [secondCityData, setSecondCityData] = useState(JSON);
  const [distance, setDistance] = useState(0);

  useEffect(() => {
    fetch("https://api.api-ninjas.com/v1/city?name=" + firstCity, {
      method: "GET",
      headers: { "X-Api-Key": "I4EJixADy/si187LJrrDSw==cRP3iUwPvw84pecL" }, //Burde egentlig ligge i en .env fil
      contentType: "application/json",
    })
      .then((res) => res.json())
      .then((data) => setFirstCityData(data[0]));
  }, [firstCity]);

  useEffect(() => {
    fetch("https://api.api-ninjas.com/v1/city?name=" + secondCity, {
      method: "GET",
      headers: { "X-Api-Key": "I4EJixADy/si187LJrrDSw==cRP3iUwPvw84pecL" },
      contentType: "application/json",
    })
      .then((res) => res.json())
      .then((data) => setSecondCityData(data[0]));
  }, [secondCity]);

  const calculateDistance = () => {
    let lat1 = firstCityData.latitude;
    let lat2 = secondCityData.latitude;
    let lon1 = firstCityData.longitude;
    let lon2 = secondCityData.longitude;
    // The math module contains a function
    // named toRadians which converts from
    // degrees to radians.
    lon1 = (lon1 * Math.PI) / 180;
    lon2 = (lon2 * Math.PI) / 180;
    lat1 = (lat1 * Math.PI) / 180;
    lat2 = (lat2 * Math.PI) / 180;

    // Haversine formula
    let dlon = lon2 - lon1;
    let dlat = lat2 - lat1;
    let a =
      Math.pow(Math.sin(dlat / 2), 2) +
      Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dlon / 2), 2);

    let c = 2 * Math.asin(Math.sqrt(a));

    // Radius of earth in kilometers. Use 3956
    // for miles
    let r = 6371;

    // calculate the result
    const avstand = Math.round(c * r);

    setDistance(avstand);
  };

  return (
    <div className="App">
      <header className="App-header">
        <div>
          <form>
            <label>
              <h2>First city</h2>
              <input
                type="text"
                className='inputClass'
                onChange={(e) => setFirstCity(e.target.value)}
              ></input>
            </label>
            <label>
            <h2>Second city</h2>
              <input
                type="text"
                className='inputClass'
                onChange={(e) => setSecondCity(e.target.value)}
              ></input>
            </label>
          </form>
          <p></p>
          <button className="buttonClass" onClick={calculateDistance}>
            Kalkuler distanse
          </button>
        </div>
        <div>{distance === 0 ? "" : <h3>{distance} km</h3>}</div>
      </header>
    </div>
  );
}

export default App;
